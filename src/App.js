
import './App.css';
import Mynav from './components/Mynav';

import 'bootstrap/dist/css/bootstrap.min.css'
import Counter from './pages/Counter';
function App() {
  return (
    <>
       <Mynav/>
       <Counter/>
    </>
  );
}

export default App;
